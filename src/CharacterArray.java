import java.util.Scanner;

public class CharacterArray {
	
	static String alphaNumeric;;
	static char[] characterArray;
	
	public static void main(String[] args) {
		getInput();
		toArray();
	}
	
	//Gets keyboard input from user
	public static void getInput() {
		System.out.println("Please enter an Alpha-Numeric String: ");
		Scanner input = new Scanner(System.in);
		alphaNumeric = input.nextLine();
	}
	
	public static void toArray() {
		characterArray = new char[alphaNumeric.length()];
		for(int i = 0; i < alphaNumeric.length(); i++) {
			//if it's a number, print out an * alongside the number
			if (Character.isDigit(alphaNumeric.charAt(i))) {
				characterArray[i] = alphaNumeric.charAt(i);
				System.out.println(characterArray[i] + " is a digit *");
			//if its uppercase, print it out as a lowercase
			} else if (Character.isUpperCase(alphaNumeric.charAt(i))) {
				characterArray[i] = alphaNumeric.charAt(i);
				System.out.println(Character.toLowerCase(characterArray[i]));
			//only remaining possibility is a lowercase character, which is printed in uppercase
			} else {
				characterArray[i] = alphaNumeric.charAt(i);
				System.out.println(Character.toUpperCase(characterArray[i]));
			}
		}
	}
}
